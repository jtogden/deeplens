provider "aws" {}

# Create roles for AWS DeepLens Device and attach policies

resource "aws_iam_role" "AWSDeepLensServiceRole" {
  name = "AWSDeepLensServiceRole"
  description = "Role for DeepLens"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "deeplens.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "AWSDeepLensServiceRole-Attach" {
    role       = "${aws_iam_role.AWSDeepLensServiceRole.name}"
    policy_arn = "arn:aws:iam::aws:policy/service-role/AWSDeepLensServiceRolePolicy"
}

resource "aws_iam_role" "AWSDeepLensGreengrassRole" {
  name = "AWSDeepLensGreengrassRole"
  description = "Role for DeepLens - Greengrass"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "greengrass.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "AWSDeepLensGreengrassRole-Attach" {
    role       = "${aws_iam_role.AWSDeepLensGreengrassRole.name}"
    policy_arn = "arn:aws:iam::aws:policy/service-role/AWSGreengrassResourceAccessRolePolicy"
}

resource "aws_iam_role" "AWSDeepLensGreengrassGroupRole" {
  name = "AWSDeepLensGreengrassGroupRole"
  description = "Role for DeepLens - Greengrass"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "greengrass.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "AWSDeepLensGreengrassGroupRole-Attach" {
    role       = "${aws_iam_role.AWSDeepLensGreengrassGroupRole.name}"
    policy_arn = "arn:aws:iam::aws:policy/AWSDeepLensLambdaFunctionAccessPolicy"
}

resource "aws_iam_role" "AWSDeepLensSagemakerRole" {
  name = "AWSDeepLensSagemakerRole"
  description = "Role for DeepLens - SageMaker"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "sagemaker.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "AWSDeepLensSagemakerRole-Attach" {
    role       = "${aws_iam_role.AWSDeepLensSagemakerRole.name}"
    policy_arn = "arn:aws:iam::aws:policy/AmazonSageMakerFullAccess"
}

resource "aws_iam_role" "AWSDeepLensLambdaRole" {
  name = "AWSDeepLensLambdaRole"
  description = "Role for DeepLens - Lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
