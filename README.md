# DeepLensSetupTerraform

Terraform plan for to create deeplens Iam roles based on - http://docs.aws.amazon.com/deeplens/latest/dg/deeplens-user-and-roles.html

### How to
Clone repo to where ever
```
$ git clone git@gitlab.com:JonTOgden/deeplens.git
```

Set your AWS env variables
```
$ export AWS_ACCESS_KEY_ID="<<ACCESS_KEY_HERE>>"
$ export AWS_SECRET_ACCESS_KEY="<<SECRET_KEY_HERE>>"
$ export AWS_DEFAULT_REGION="us-east-1"
```
Initialize terraform to install AWS modules
```
$ terraform init
```

Validate what terraform wants to change
```
$ terraform plan -out=plan
```

Apply terraform plan
```
$ terraform apply plan
```

Remember to keep track of your state file for future changes
